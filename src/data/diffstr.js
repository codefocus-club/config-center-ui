export default `
--- a/AwesomeWalle.Business/Jira/IssueBusiness.cs
+++ b/AwesomeWalle.Business/Jira/IssueBusiness.cs
@@ -11,6 +11,7 @@ namespace AwesomeWalle.Business.Jira
 {
     public class IssueBusiness
     {
+        private const string JIRA_USER_ISSUES_KEY_PREFIX = "Manager_";
         public IssueResponse GetUerIssues(string workId, int error = 0)
         {
             try
@@ -104,13 +105,12 @@ namespace AwesomeWalle.Business.Jira
         {
             try
             {
-                string key = "Manager_";
                 string newWorkId = workId;
                 if (workId.Length == 7 && "10,11,12".Contains(workId.Substring(0, 2)))
                 {
                     newWorkId = workId.Substring(2, 5);
                 }
-                var issues = JiraCache.Instance().Get(key + workId);
+                var issues = JiraCache.Instance().Get(JIRA_USER_ISSUES_KEY_PREFIX + workId);
                 if (issues.IsNotNull() && issues.Issues.HasAny())
                 {
                     return issues;
@@ -125,7 +125,7 @@ namespace AwesomeWalle.Business.Jira
                     $"AND resolution = Unresolved AND  (技术负责人 in ('{newWorkId}','{workId}')" +
                     $"OR DEV in ('{newWorkId}','{workId}') )   ORDER BY priority DESC, updated DESC";
                 var result = new IssueClient().GetIssueByJql(req);
-                JiraCache.Instance().Set(key + workId, result);
+                JiraCache.Instance().Set(JIRA_USER_ISSUES_KEY_PREFIX + workId, result);
                 return result;
             }
             catch (Exception ex)
@@ -143,5 +143,11 @@ namespace AwesomeWalle.Business.Jira
                 }
             }
         }
+
+        public bool DeleteCache(string workId,bool isManager)
+        {
+            var token = isManager ? JIRA_USER_ISSUES_KEY_PREFIX + workId.Trim() : workId.Trim();
+            return JiraCache.Instance().Delete(token);
+        }
     }
 }
`
