import { login, logout, getInfo, getUserList,addUser,updateUser,deleteUser } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif'
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login({ username: username.trim(), password: password }).then(response => {
        const data = response.result
        commit('SET_TOKEN', data.token)
        commit('SET_NAME', data.user.username)
        setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        // const { data } = response.result
        // console.log(response.result, '---')
        commit('SET_NAME', response.result.username)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      // logout(state.token).then(() => {
      removeToken() // must remove  token  first
      resetRouter()
      commit('RESET_STATE')
      resolve()
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  },
  getUserList({ commit }, data) {
    // console.log(pageIndex,pageSize,'1111111')
    const { pageIndex, pageSize } = data
    return new Promise((resolve, reject) => {
      getUserList({ pageIndex: pageIndex, pageSize: pageSize }).then(response => {
        resolve(response)
      }).catch(error => {
        console.log(1, error)
        reject(error)
      })
    })
  },
  addUser({ commit }, userInfo) {
    const { username, pass } = userInfo
    return new Promise((resolve, reject) => {
      addUser({ username: username, password: pass }).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  updateUser({ commit }, data) {
    const {password,id}=data;
    return new Promise((resolve, reject) => {
      updateUser({password:password,id:id}).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  deleteUser({ commit }, id) {
    return new Promise((resolve, reject) => {
      deleteUser({ id:id }).then(response => {
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

