import defaultSettings from '@/settings'

const title = defaultSettings.title || 'CodeFocus配置中心'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
