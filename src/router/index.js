import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  },
  {
    path: '/configManager',
    component: Layout,
    redirect: '/configManager/config',
    name: 'Manager',
    meta: { title: '管理中心', icon: 'example' },
    children: [
      {
        path: 'config',
        name: 'Config',
        component: () => import('@/views/table/index'),
        meta: { title: '配置列表', icon: 'component' }
      },
      {
        path: 'list/:id(\\d+)',
        component: () => import('@/views/table/list'),
        name: 'list',
        meta: { title: '历史版本', noCache: true, activeMenu: '/configManager/config' },
        hidden: true
      },
      {
        path: 'index',
        name: 'Form2',
        component: () => import('@/views/form/index'),
        meta: { title: '待定', icon: 'nested' }
      }
    ]
  },
  {
    path: '/userManager',
    component: Layout,
    redirect: '/userManager/table',
    name: 'UserManager',
    meta: { title: '用户中心', icon: 'user' },
    children: [
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/table/UserList'),
        meta: { title: '用户列表', icon: 'user' }
      },
      {
        path: 'index',
        name: 'Form2',
        component: () => import('@/views/form/index'),
        meta: { title: '待定', icon: 'nested' }
      }
    ]
  },
  {
    path: '/icon',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/icons/index'),
        name: 'Icons',
        meta: { title: 'Icons', icon: 'icon', noCache: true }
      }
    ]
  },
  {
    path: 'external-link',
    component: Layout,
    children: [
      {
        path: 'https://codefocus.club',
        meta: { title: 'About CodeFocus', icon: 'link' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
