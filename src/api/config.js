import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/config/getConfigList',
    method: 'get',
    params
  })
}

export function saveConfig(data) {
  return request({
    url: '/config/saveConfigModel',
    method: 'post',
    data
  })
}

export function deleteConfigId(params) {
  return request({
    url: '/config/deleteConfigId',
    method: 'post',
    params
  })
}

export function updateConfig(data) {
  return request({
    url: '/config/updateConfigModel',
    method: 'post',
    data
  })
}


export function getConfigHistoryList(params) {
  return request({
    url: '/config/getConfigHistoryList',
    method: 'get',
    params
  })
}



export function getConfigHistoryInfo(params) {
  return request({
    url: '/config/getConfigHistoryInfo',
    method: 'get',
    params
  })
}
